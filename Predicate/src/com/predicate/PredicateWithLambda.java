package com.predicate;

import java.util.function.Predicate;

public class PredicateWithLambda {
    public static void main(String[] args) {
        Predicate<Integer> predicate = number->{
          if (number>10)
              return true;
          else
              return false;
        };
        System.out.println(predicate.test(5));
    }
}
// predicte return type will be boolean